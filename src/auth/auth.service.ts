import { Injectable } from '@nestjs/common';
import { AWSError } from 'aws-sdk';
import CognitoIdentityServiceProvider, {
  AttributeType,
  ForgotPasswordRequest,
  InitiateAuthRequest,
  InitiateAuthResponse,
  SignUpRequest,
} from 'aws-sdk/clients/cognitoidentityserviceprovider';
import { PromiseResult } from 'aws-sdk/lib/request';
import * as crypto from 'crypto';
import { AWSCognitoProvider } from 'src/shared/providers/AWSCognitoProvider';
import { EnvProvider } from 'src/shared/providers/EnvProvider';
import { SignInDto } from './dto/SignInDto';
import { UserCreateDto } from './dto/UserCreateDto';
@Injectable()
export class AuthService {
  constructor(
    private awsCognitoProvider: AWSCognitoProvider,
    private envProvider: EnvProvider,
  ) {}
  private generateHash(username: string): string {
    return crypto
      .createHmac(
        'SHA256',
        this.envProvider.cognitoCredentials.COGNITO_CLIENT_SECRET,
      )
      .update(username + this.envProvider.cognitoCredentials.COGNITO_CLIENT_ID)
      .digest('base64');
  }
  public async signUp(data: UserCreateDto) {
    const cognitoAttributes: AttributeType[] = [];
    const { password, email, ...others } = data;
    Object.keys(others).forEach((el) =>
      cognitoAttributes.push({ Name: el, Value: others[el] }),
    );
    // console.log("Generated hash: ", this.generateHash(email));
    const params: SignUpRequest = {
      ClientId: this.envProvider.cognitoCredentials.COGNITO_CLIENT_ID,
      Password: password,
      Username: email,
      SecretHash: this.generateHash(email),
      UserAttributes: cognitoAttributes,
    };

    try {
      const data = await this.awsCognitoProvider.cognitoIdentityServiceProvider
        .signUp(params)
        .promise();
      return data;
    } catch (error) {
      console.log(error);
      throw error as Error;
    }
  }
  public async forgotPassword(email: string) {
    const params: ForgotPasswordRequest = {
      ClientId: this.envProvider.cognitoCredentials.COGNITO_CLIENT_ID,
      Username: email,
      SecretHash: this.generateHash(email),
    };
    try {
      await this.awsCognitoProvider.cognitoIdentityServiceProvider
        .forgotPassword(params)
        .promise();
      return;
    } catch (err) {
      console.error(err);
    }
    return;
  }

  public async signIn(credentials: SignInDto) {
    const { email, password } = credentials;
    const params: InitiateAuthRequest = {
      AuthFlow: 'USER_PASSWORD_AUTH',
      ClientId: this.envProvider.cognitoCredentials.COGNITO_CLIENT_ID,
      AuthParameters: {
        USERNAME: email,
        PASSWORD: password,
        SECRET_HASH: this.generateHash(email),
      },
    };
    try {
      const response =
        (await this.awsCognitoProvider.cognitoIdentityServiceProvider
          .initiateAuth(params)
          .promise()) as InitiateAuthResponse;

      return response;
    } catch (err) {
      throw new Error(err.message);
    }
  }

  public async decodeToken(token: string){
    try{
      //https://www.npmjs.com/package/@southlane/cognito-jwt-verifier
    }catch{

    }
    return;
  }
}
