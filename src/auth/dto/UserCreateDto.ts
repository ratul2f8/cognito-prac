import {
  IsEmail,
  IsNotEmpty,
  MinLength,
} from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
export class UserCreateDto {
  @IsNotEmpty()
  @MinLength(2)
  @ApiProperty({ required: true, description: 'Name of the user' })
  name: string;

  @IsNotEmpty()
  @IsEmail()
  @ApiProperty({ required: true, description: 'Email of the user' })
  email: string;

  @IsNotEmpty()
  @MinLength(6)
  @ApiProperty({
    required: true,
    description: 'Password of the user',
  })
  password: string;
}
