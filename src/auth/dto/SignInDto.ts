import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsEmail, MinLength } from 'class-validator';
export class SignInDto {
  @IsNotEmpty()
  @IsEmail()
  @ApiProperty({ required: true, description: 'Email of the user' })
  email: string;

  @IsNotEmpty()
  @MinLength(6)
  @ApiProperty({
    required: true,
    description: 'Password of the user',
  })
  password: string;
}
