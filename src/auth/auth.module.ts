import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { EnvProvider } from 'src/shared/providers/EnvProvider';
import { AWSCognitoProvider } from 'src/shared/providers/AWSCognitoProvider';
@Module({
  providers: [AuthService, EnvProvider, AWSCognitoProvider],
  controllers: [AuthController]
})
export class AuthModule {}
