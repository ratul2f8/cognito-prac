import {
  Body,
  ConflictException,
  Controller,
  NotFoundException,
  Post,
} from '@nestjs/common';
import {
  ApiBadRequestResponse,
  ApiConflictResponse,
  ApiCreatedResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiTags,
} from '@nestjs/swagger';
import { BadRequestErrorClass } from 'src/shared/types/error-classes';
import { AuthService } from './auth.service';
import { ForgotPasswordDto } from './dto/ForgotPasswordDto';
import { SignInDto } from './dto/SignInDto';
import { UserCreateDto } from './dto/UserCreateDto';
@ApiTags('Authentication')
@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @ApiBadRequestResponse({ type: BadRequestErrorClass })
  @ApiConflictResponse()
  @ApiCreatedResponse()
  @Post('/signup')
  public async signUp(@Body() body: UserCreateDto) {
    try {
      const response = await this.authService.signUp(body);
      return response;
    } catch (err) {
      throw new ConflictException(err.message);
    }
  }

  @ApiOkResponse()
  @ApiBadRequestResponse({ type: BadRequestErrorClass })
  @Post('/forgot-password')
  public async forgotPassword(@Body() body: ForgotPasswordDto) {
    try{
      await this.authService.forgotPassword(body.email);
    }catch(err){
      console.error(err);
    }
    return 'Message will be sent';
  }

  @ApiNotFoundResponse()
  @ApiBadRequestResponse({ type: BadRequestErrorClass })
  @ApiOkResponse()
  @Post('/signin')
  public async signIn(@Body() body: SignInDto) {
    try {
      const data = await this.authService.signIn(body);
      return data;
    } catch (err) {
      // console.error("Response error", err)
      throw new NotFoundException(err.message);
    }
  }
}
