import { ApiProperty } from "@nestjs/swagger";

export class BadRequestErrorClass{
    @ApiProperty()
    statusCode: number;
    @ApiProperty() 
    message: string[];
    @ApiProperty() 
    error: string;
}