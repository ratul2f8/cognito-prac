export interface ICognitoEnv {
  COGNITO_CLIENT_ID: string;
  COGNITO_CLIENT_SECRET: string;
  COGNITO_USER_POOL_ID: string;
  COGNITO_USER_POOL_REGION: string;
}

export interface IAWSEnv {
  AWS_ACCESS_KEY: string;
  AWS_SECRET_ACCESS_KEY: string;
}
