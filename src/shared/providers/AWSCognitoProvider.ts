import { Injectable } from '@nestjs/common';
import * as AWS from 'aws-sdk';
import { EnvProvider } from './EnvProvider';

@Injectable()
export class AWSCognitoProvider {
  cognitoIdentityServiceProvider: AWS.CognitoIdentityServiceProvider;

  cognitoClientConfiguration: AWS.CognitoIdentityServiceProvider.ClientConfiguration;

  constructor(private envProvider: EnvProvider) {
    this.cognitoClientConfiguration = {
      apiVersion: 'latest',
      credentials: {
        accessKeyId: this.envProvider.awsCredentials.AWS_ACCESS_KEY,
        secretAccessKey: this.envProvider.awsCredentials.AWS_SECRET_ACCESS_KEY,
      },
      region: this.envProvider.cognitoCredentials.COGNITO_USER_POOL_REGION,
    };
    this.cognitoIdentityServiceProvider =
      new AWS.CognitoIdentityServiceProvider(this.cognitoClientConfiguration);
  }
}
