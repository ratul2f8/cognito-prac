import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { IAWSEnv, ICognitoEnv } from 'src/shared/types/interfaces';

@Injectable()
export class EnvProvider {
  cognitoCredentials: ICognitoEnv;
  awsCredentials: IAWSEnv;

  constructor(private configService: ConfigService) {
    this.cognitoCredentials = {
      COGNITO_CLIENT_ID: this.configService.get('COGNITO_CLIENT_ID'),
      COGNITO_CLIENT_SECRET: this.configService.get('COGNITO_CLIENT_SECRET'),
      COGNITO_USER_POOL_ID: this.configService.get('COGNITO_USER_POOL_ID'),
      COGNITO_USER_POOL_REGION: this.configService.get(
        'COGNITO_USER_POOL_REGION',
      ),
    };
    this.awsCredentials = {
      AWS_ACCESS_KEY: this.configService.get('AWS_ACCESS_KEY'),
      AWS_SECRET_ACCESS_KEY: this.configService.get('AWS_SECRET_ACCESS_KEY'),
    };
  }
}
